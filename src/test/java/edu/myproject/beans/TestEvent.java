package edu.myproject.beans;

import edu.myproject.beans.Event;
import org.junit.Test;

import java.text.DateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class TestEvent {

  @Test
  public void testToStringDate() {
    Date date = new Date();
    DateFormat dateFormat = DateFormat.getDateInstance();
    Event event= new Event(date,dateFormat);

    String strTest = event.toString();
    assertTrue(strTest.contains(dateFormat.format(date)));
  }


  @Test
  public void testToStringDateTime() {
    Date date = new Date();
    DateFormat dateFormat = DateFormat.getDateTimeInstance();
    Event event= new Event(date,dateFormat);

    String strTest = event.toString();
    assertTrue(strTest.contains(dateFormat.format(date)));
  }
}