package edu.myproject.loggers;

import edu.myproject.beans.Event;
import edu.myproject.loggers.CacheFileEventLogger;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

import static org.junit.Assert.assertTrue;

public class TestCacheFileEventLogger {
  private File file;

  @Before
  public void setUp() throws Exception {
    this.file = File.createTempFile("test", "edu.myproject.loggers.CacheFileEventLogger");
  }

  @After
  public void tearDown() throws Exception {
    file.delete();
  }

  @Test
  public void testLogEvent() throws IOException {
    Event event = new Event(new Date(), DateFormat.getDateTimeInstance());
    int cacheSize = 3;
    CacheFileEventLogger fileLogger = new CacheFileEventLogger(file.getAbsolutePath(), cacheSize);
    fileLogger.init();

    // verify that file is empty
    String content = FileUtils.readFileToString(this.file, "ISO-8859-1");
    assertTrue("File is empty after init", content.isEmpty());

    // add event to cache
    fileLogger.logEvent(event);
    fileLogger.logEvent(event);
    content = FileUtils.readFileToString(this.file, "ISO-8859-1");
    assertTrue("File is empty, event in cache ", content.isEmpty());

    fileLogger.logEvent(event);
    content = FileUtils.readFileToString(this.file, "ISO-8859-1");
    assertTrue("File not empty, event in file ", !content.isEmpty());
  }

  @Test
  public void testDestroy() throws IOException {
    Event event = new Event(new Date(), DateFormat.getDateTimeInstance());
    int cacheSize = 2;
    CacheFileEventLogger fileLogger = new CacheFileEventLogger(file.getAbsolutePath(), cacheSize);
    fileLogger.init();

    String content = FileUtils.readFileToString(this.file, "ISO-8859-1");
    assertTrue("File is empty after init", content.isEmpty());

    fileLogger.logEvent(event);
    content = FileUtils.readFileToString(this.file, "ISO-8859-1");
    assertTrue("File is empty, event in cache ", content.isEmpty());

    // verify destroy logic
    fileLogger.destroy();
    content = FileUtils.readFileToString(this.file, "ISO-8859-1");
    assertTrue("File not empty, cache was saved", !content.isEmpty());
  }
}
