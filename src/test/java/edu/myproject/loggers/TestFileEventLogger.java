package edu.myproject.loggers;

import edu.myproject.beans.Event;
import edu.myproject.loggers.FileEventLogger;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class TestFileEventLogger {
  private File file;

  @Before
  public void createFile() throws IOException {
    this.file = File.createTempFile("test", "edu.myproject.loggers.FileEventLogger");
  }

  @After
  public void deleteFile() {
    file.delete();
  }

  @Test
  public void testInit() throws IOException {
    FileEventLogger fileEventLogger = new FileEventLogger(file.getAbsolutePath());
    fileEventLogger.init();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInitFail() throws IOException {
    FileEventLogger fileEventLogger = new FileEventLogger(file.getAbsolutePath());

    // set readonly attribute for test file
    file.setReadOnly();
    fileEventLogger.init();
  }

  @Test
  public void testLogEvent() throws IOException {
    Event event = new Event(new Date(), DateFormat.getDateTimeInstance());
    FileEventLogger fileEventLogger = new FileEventLogger(file.getAbsolutePath());
    fileEventLogger.init();

    String content = FileUtils.readFileToString(file, "ISO-8859-1");
    assertTrue("File is empty", content.isEmpty());

    fileEventLogger.logEvent(event);
    content = FileUtils.readFileToString(file, "ISO-8859-1");
    assertFalse("File not empty", content.isEmpty());
  }
}
