package edu.myproject;

import edu.myproject.beans.Client;
import edu.myproject.beans.Event;
import edu.myproject.beans.EventType;
import edu.myproject.loggers.EventLogger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

public class App {

  private Client client;
  private EventLogger defaultLogger;
  private Map<EventType, EventLogger> loggers;

  public App(Client client, EventLogger eventLogger, Map<EventType, EventLogger> loggers) {
    this.client = client;
    this.defaultLogger = eventLogger;
    this.loggers = loggers;
  }

  public static void main(String[] args) {
    //
    //    edu.myproject.App app = new edu.myproject.App();
    //    app.client = new edu.myproject.beans.Client("1", "Lorem ipsum");
    //    app.eventLogger = new edu.myproject.loggers.ConsoleEventLogger();
    //    app.logEvent("Some event for user 1");


    ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
    App app = (App) ctx.getBean("app");
    Client client = ctx.getBean(Client.class);
    System.out.println("edu.myproject.beans.Client says: " + client.getGreeting());

    Event event = ctx.getBean(Event.class);
    app.logEvent(EventType.INFO, event, "Some event for 1");

    event = ctx.getBean(Event.class);
    app.logEvent(EventType.ERROR, event, "Some event for 2");

    event = ctx.getBean(Event.class);
    app.logEvent(null, event, "Some event for 3");

    ctx.close();
  }

  private void logEvent(EventType type, Event event, String msg) {
    String message = msg.replaceAll(client.getId(), client.getFullName());
    event.setMsg(message);

    EventLogger logger = loggers.get(type);
    if (logger == null) {
      logger = defaultLogger;
    }

    logger.logEvent(event);
  }
}
