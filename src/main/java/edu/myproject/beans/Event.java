package edu.myproject.beans;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Event {

  private static final AtomicInteger AUTO_ID = new AtomicInteger(0);

  private int id; // auto-generated
  private String msg; // set in setter
  private Date date; // set in constructor
  private DateFormat dateFormat;

  public Event(Date date, DateFormat df) {
    this.date = date;
    this.dateFormat = df;
    this.id = AUTO_ID.getAndIncrement();
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public int getId() {
    return id;
  }

  public Date getDate() {
    return date;
  }

  @Override
  public String toString() {
    return "edu.myproject.beans.Event{"
        + "id="
        + id
        + ", msg='"
        + msg
        + '\''
        + ", date="
        + dateFormat.format(date)
        + '}';
  }
}
