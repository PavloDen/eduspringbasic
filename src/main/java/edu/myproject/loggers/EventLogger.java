package edu.myproject.loggers;

import edu.myproject.beans.Event;

public interface EventLogger {
  void logEvent(Event event);
}
