package edu.myproject.loggers;

import edu.myproject.beans.Event;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class FileEventLogger implements EventLogger {
  private String fileName;
  private File file;

  public FileEventLogger(String fileName) {
    this.fileName = fileName;
  }

  public void init() throws IOException {
    this.file = new File(fileName);

    // check file write access
    if (file.exists() && !file.canWrite()) {
      throw new IllegalArgumentException("Can not write to file: " + fileName);
    } else if (!file.exists()) {
      file.createNewFile();
    }
  }

  @Override
  public void logEvent(Event event) {
    try {

      FileUtils.writeStringToFile(file, event.toString() + "\n", "ISO-8859-1");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
